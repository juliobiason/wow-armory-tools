#!/usr/bin/python

import logging

import armory

from configopt import ConfigOpt

def main():
    logging.basicConfig(level=logging.DEBUG)

    opts = ConfigOpt()
    opts.add_group('Armory', 'Armory information to be retrieved')
    opts.add_option('-s', '--server',
        group='Armory',
        option='server',
        help='Guild server.')
    opts.add_option('-g', '--guild',
        group='Armory',
        option='guild',
        help='Guild name.')
    opts()
    
    if not opts['Armory']['server']:
        logging.error('Missing server info.')
        return
        
    if not opts['Armory']['guild']:
        logging.error('Missing guild name.')
        return
        
    results = []
        
    for member in armory.members(opts['Armory']['server'], 
            opts['Armory']['guild']):
        member_stats = armory.statistics(member, armory.stats['skills'])
        (_, value) = member_stats['1524']
    
        message = '%s (%s)' % (member['name'], value)
        if value == '450':
            logging.info(message)
            results.append(message)
    
    with file('results.txt', 'w') as f:
        for line in results:
            f.write(line + '\n')

if __name__ == '__main__':
    main()
