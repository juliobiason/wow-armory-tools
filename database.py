#!/usr/bin/env python

import sqlite3
import os
import datetime

class ArmoryDatabase(object):

    DATABASE_NAME = './armory.db'
    (KILL_COUNT, RATING2, RATING3, RATING5, LEVEL) = range(1, 6)

    def __init__(self):
        """Class initialization."""
        create_tables = not os.access(self.DATABASE_NAME, os.F_OK)
        self.conn = sqlite3.connect(self.DATABASE_NAME)
        if create_tables:
            self.create_database()

    def _create_feat(self, description):
        cursor = self.conn.cursor()
        cursor.execute('''insert into feats (id, description)
            values (NULL, ?)''', (description,))
        self.conn.commit()
        return

    def create_database(self):
        """Create a database file and the tables."""
        cursor = self.conn.cursor()
        cursor.execute('''create table characters (
            id integer primary key,
            name text,
            class text,
            race text)''')
        cursor.execute('''create table feats (
            id integer primary key,
            description text)''')
        cursor.execute('''create table updates (
            character integer,
            date text,
            feat integer,
            value integer)''')
        self.conn.commit()
        cursor.close()

        # create the list of feats
        self._create_feat('Kills')
        self._create_feat('2v2 rating')
        self._create_feat('3v3 rating')
        self._create_feat('5v5 rating')
        self._create_feat('Level')

        return

    def character_check(self, name, klass, race):
        """Check if a character exists and, if it does, return the id for it;
        if not, create a new entry and return the id."""
        cursor = self.conn.cursor()
        cursor.execute('select id from characters where name=?', (name,))
        data = cursor.fetchone()
        if data is None:
            # character doesn't exist in the database. Create it.
            cursor.execute('''insert into characters (id, name, race, class)
                values (NULL, ?, ?, ?)''', (name, race, klass))
            self.conn.commit()
            cursor.execute('''select last_insert_rowid()''')
            data = cursor.fetchone()

        id = data[0]
        cursor.close()

        return id

    def update(self, character, feat, value):
        today = datetime.date.today()
        cursor = self.conn.cursor()
        cursor.execute('''insert into updates (character, date, feat, value)
            values (?, ?, ?, ?)''', (character, str(today), feat, value))
        self.conn.commit()
        cursor.close()
        return
