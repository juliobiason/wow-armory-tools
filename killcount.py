#!/usr/bin/env python

from xml.etree.ElementTree import ElementTree
import urllib
import urllib2
import time
import logging

from database import ArmoryDatabase
from configopt import ConfigOpt

AGENT = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.1) Gecko/20061204 Firefox/2.0.0.1"
BASE = "http://www.wowarmory.com/"

def get_data(page, info):
    url = BASE + page + '?' + info

    tree = ElementTree()
    request = urllib2.Request(url=url, 
            headers={'User-agent': AGENT})
    try:
        content = urllib2.urlopen(request)
        tree.parse(content)
        content.close()
    except urllib2.URLError:
        # consider this a temporary error; sleep for a while and then tries
        # again
        print 'Connection failure. Sleeping for 3 seconds...'
        time.sleep(3)
        print 'Trying again.'
        return get_data(page, info)

    root = tree.getroot()

    return root


def get_character_info(url):
    """Retrive the information about a character"""
    root = get_data('character-sheet.xml', url)
    result = {}

    char_info = root.find('characterInfo/character')
    if char_info is None:
        print 'No info'
        return result

    if 'points' in char_info.attrib:
        achievements = char_info.attrib['points']
    else:
        achievements = 0
    print '\tAchievement:', achievements
    print '\tLevel:', char_info.attrib['level']
    result[ArmoryDatabase.LEVEL] = int(char_info.attrib['level'])

    arenas = root.find('characterInfo/character/arenaTeams')
    if not arenas is None:
        for arena_team in arenas.getchildren():
            team_size = int(arena_team.attrib['size'])
            team_rating = int(arena_team.attrib['rating'])
            print '\t%dv%d: %d' % (team_size, team_size, team_rating)

            if team_size == 2:
                key = ArmoryDatabase.RATING2
            elif team_size == 3:
                key = ArmoryDatabase.RATING3
            else:
                key = ArmoryDatabase.RATING5

            result[key] = team_rating

    kills = root.find('characterInfo/characterTab/pvp/lifetimehonorablekills')
    if kills is None:
        kill_count = 0
    else:
        kill_count = int(kills.attrib['value'])
    print '\tKills:', kill_count
    result[ArmoryDatabase.KILL_COUNT] = kill_count
    return result


def main():
    opts = ConfigOpt()
    opts.add_group('Database', 'Database options')
    opts.add_option('-u', '--user',
            group='Database',
            option='user',
            help='Username used to connect to the database.')
    opts.add_option('-p', '--password',
            group='Database',
            option='password',
            help='Database password.')
    opts.add_option('--host',
            group='Database',
            option='host',
            help='Database host')

    opts.add_group('Armory', 'Armory information to be retrieved')
    opts.add_option('-s', '--server',
            group='Armory',
            option='server',
            help='Guild server.')
    opts.add_option('-g', '--guild',
            group='Armory',
            option='Guild name.')

    opts()

    if not opts['Armory']['server']:
        logging.error('Missing server info.')
        return

    if not opts['Armory']['guild']:
        logging.error('Missing guild name.')
        return


    if 0:
        database = ArmoryDatabase()
        server = urllib.quote_plus('Firetree')
        guild = urllib.quote_plus('Desperate Hordewives')

        root = get_data('guild-info.xml', 'r=%s&n=%s' % (server, guild))

        members = root.find('guildInfo/guild/members')
        count = 1
        result = {}

        for member in members.getiterator():
            member_info = member.attrib

            if not 'name' in member_info:
                # probably some other information about the guild hoster.
                continue

            name = member_info['name']
            race = member_info['race']
            klass = member_info['class']

            print name + ',', race, klass
            result[name] = {}
            result[name]['race'] = race
            result[name]['class'] = klass
            result[name]['feats'] = get_character_info(member_info['url'])

        # save the results in the database
        for name in result:
            id = database.character_check(name, result[name]['class'],
                    result[name]['race'])

            feats = result[name]['feats']
            for feat in feats:
                database.update(id, feat, feats[feat])

    print 'Done'
if __name__ == '__main__':
    main()
