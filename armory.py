#!/usr/bin/python

import logging
import urllib
import urllib2
import time

from xml.etree.ElementTree import ElementTree

# If we don't set a "proper" user agent, armory won't return a proper XML
# (seems they return an HTML, in that case)
USER_AGENT = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.1) Gecko/20061204 Firefox/2.0.0.1"

URL_BASE = 'http://www.wowarmory.com/'

# list of statistics you can retrieve for the user.
stats = {
    'character': 130,
    'combat': 141,
    'kills': 128,
    'deaths': 122,
    'quests': 133,
    'dungeons&raids': 14807,
    'skills': 132,
    'travel': 134,
    'social': 131,
    'playervsplayer': 21
    }

def _retrieve(page, info, max_tries=3):
    """Retrieve information from the armory. Returns a ElementTree object 
    with the response from the server."""
    
    url = URL_BASE + page + '.xml?' + info
    
    logging.debug('Requesting %s', url)
    
    request = urllib2.Request(url=url,
            headers={'User-agent': USER_AGENT})
            
    try:
        content = urllib2.urlopen(request)
        tree = ElementTree()
        tree.parse(content)
        content.close()
    except urllib2.URLError, exc:
        # sometimes, armory fails with no good reason, but it seems due 
        # high amount of requests or something. sleeping for a while and
        # trying again works most of the time.
        logging.error('Connection failed, still have %d tries', max_tries)
        if max_tries == 0:
            raise urllib2.URLError(exc)
            return
            
        time.sleep(3)   # 3 secondss
        
        return _retrieve(page, info, max_tries-1)
    
    #tree.write('stats.xml')
    root = tree.getroot()
    
    return root
    
def members(server, guild):
    """Retrieve the list of guild members. Returns a dictionary with some
    basic info as a dictionary."""
    
    server = urllib.quote_plus(server)
    guild = urllib.quote_plus(guild)
    
    data = _retrieve('guild-info', 'r=%s&n=%s' % (server, guild))
    
    members = data.find('guildInfo/guild/members')
    
    for member in members.getiterator():
        member_info = member.attrib
        
        if not 'name' in member_info:
            # sometimes, some other guild information appears in the members
            # list. not sure why (or what it is, to be honest.)
            continue
            
        yield member_info
        
def _fill(result_set, element):
    """Process the element and put the results in the result_set."""
    
    for elem in element.getiterator():
        if not 'id' in elem.attrib:
            continue
        
        stat_id = elem.attrib['id']
        stat_name = elem.attrib['name']
        stat_value = elem.attrib['quantity']
        
        result_set[stat_id] = (stat_name, stat_value)
    
    return
        
def statistics(member, category):
    """Retrieve the statistics for the user. <member> is the dictionary
    returned by <members()>. category is the category ID; there is a 
    dictionary with all available categories in <stats>."""
    
    get_stats = '%s&c=%d' % (member['url'], category)
    data = _retrieve('character-statistics', get_stats)
    
    results = {}
    
    _fill(results, data)
    
    return results